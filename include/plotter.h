//from std
#include <string>
#include <iostream>
#include <memory>
//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/optional.hpp>
//from ROOT
#include "TStyle.h"
#include "TString.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TAxis.h"
#include "TLegend.h"
//from ROOFIT
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooHist.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"
//local
#include "lhcbStyle.C"

namespace pt = boost::property_tree;
using namespace std ;
using namespace RooFit ;
using namespace RooStats ;

void plotter(const RooWorkspace& ws, TFile* outfile, const TString &workdir, const pt::ptree& configtree, const char* nodename){

  outfile->cd();
  RooRealVar* Observable = ws.var(configtree.get<string>( TString::Format("plots.%s.var",nodename).Data() ).c_str());
  // set basic plot style
  lhcbStyle();

  const float textsize       = static_cast<float>(configtree.get(TString::Format("plots.%s.textsize",nodename).Data(),0.05));
  const float Left_margin    = static_cast<float>(configtree.get(TString::Format("plots.%s.lmargin",nodename).Data(),0.11));
  const float Top_margin     = static_cast<float>(configtree.get(TString::Format("plots.%s.tmargin",nodename).Data(),0.01));
  const float Right_margin   = static_cast<float>(configtree.get(TString::Format("plots.%s.rmargin",nodename).Data(),0.01));
  const float Bigy_margin    = static_cast<float>(configtree.get(TString::Format("plots.%s.bmargin",nodename).Data(),0.12));
  const float padsplit       = static_cast<float>(configtree.get(TString::Format("plots.%s.pullheight",nodename).Data(),0.32));
  const float padsplitmargin = 1e-6;//leave this hardcoded as it's the only thing making sense in this layout

  TCanvas c1("c1","Fit",static_cast<int>(configtree.get(TString::Format("plots.%s.CanvXSize",nodename).Data(),2048))
             ,static_cast<int>(configtree.get(TString::Format("plots.%s.CanvYSize",nodename).Data(),1280)));

  //Must control the margins in the individual pads and not the subpads of the canvas, because of the axis labels
  gPad->SetTopMargin   (padsplitmargin);
  gPad->SetBottomMargin(padsplitmargin);
  gPad->SetRightMargin (padsplitmargin);
  gPad->SetLeftMargin  (padsplitmargin);

  TPad pad1("pad1","pad1",padsplitmargin,padsplit,1-padsplitmargin,1-padsplitmargin);
  pad1.Draw();
  pad1.cd();
  pad1.SetBorderSize  (0);
  pad1.SetLeftMargin  (Left_margin);
  pad1.SetRightMargin (Right_margin);
  pad1.SetTopMargin   (Top_margin);
  pad1.SetBottomMargin(padsplitmargin);
  pad1.SetTickx();
  pad1.SetTicky();
  if(configtree.get_optional<bool>(TString::Format("plots.%s.logy",nodename).Data()))
    pad1.SetLogy();
  c1.Update();

  //get data and model from workspace
  RooDataSet* ds = dynamic_cast<RooDataSet*>(ws.data(static_cast<string>(configtree.get("dsName","Dred")).c_str()));
  RooAbsPdf* modelPdf = ws.pdf(configtree.get<string>("pdf").c_str());

  RooPlot* frame = Observable->frame(configtree.get<double>(TString::Format("plots.%s.low",nodename).Data()),
                                configtree.get<double>(TString::Format("plots.%s.high",nodename).Data()),
                                configtree.get<int>   (TString::Format("plots.%s.bins",nodename).Data()));

  ds->plotOn(frame,
             MarkerSize (static_cast<float>(configtree.get(TString::Format("plots.%s.MSizeData",nodename).Data(),1.f))),
             MarkerColor(LHCb::color(configtree.get(TString::Format("plots.%s.MColorData",nodename).Data(),"black"))),
             LineWidth  (static_cast<short>(configtree.get(TString::Format("plots.%s.LWidthData",nodename).Data(),1.f))),
             LineColor  (LHCb::color(configtree.get(TString::Format("plots.%s.LColorData",nodename).Data(),"black"))),
             DrawOption (static_cast<string>(configtree.get(TString::Format("plots.%s.OptionData",nodename).Data(),"e1p")).c_str())
             );

  //add pdf projections
  for(const auto& v: configtree.get_child(TString::Format("plots.%s.graphs",nodename).Data())) {
    RooLinkedList ledlist;
    vector<RooCmdArg*> args;
    if(v.second.get_optional<bool>("vlines"))    args.push_back( new RooCmdArg(VLines()) );
    if(v.second.get_optional<bool>("invisible")) args.push_back( new RooCmdArg(Invisible()) );
    args.push_back(new RooCmdArg(Components(v.second.get<string>("components").c_str()))); //the component has to be given!
    args.push_back(new RooCmdArg(Name      (TString::Format("%sP",v.second.get<string>("components").c_str()).Data())));
    args.push_back(new RooCmdArg(LineColor (LHCb::color(v.second.get("linecolor","blue"))))); //set color to blue if not specified
    args.push_back(new RooCmdArg(LineStyle (v.second.get("style",1))));
    args.push_back(new RooCmdArg(LineWidth (static_cast<Width_t>(v.second.get("linewidth",3.f)))));
    args.push_back(new RooCmdArg(FillColor (LHCb::color(v.second.get("fillcolor","blue")))));
    args.push_back(new RooCmdArg(DrawOption(static_cast<string>(v.second.get("option","L")).c_str())));
    args.push_back(new RooCmdArg(AddTo     (static_cast<string>(v.second.get("addto","")).c_str(),1,1)));
    for (auto arg : args){
      arg->setProcessRecArgs(true,false);
      ledlist.Add(arg);
    }
    modelPdf->plotOn(frame,ledlist);
    for (auto arg : args)
      delete arg;
    args.clear();
  }

  //draw second time since pdf projections may overlay parts of the data
  ds->plotOn(frame,
             Name       (TString::Format("%sP",ds->GetName()).Data()),
             MarkerSize (static_cast<float>(configtree.get(TString::Format("plots.%s.MSizeData",nodename).Data(),1.f))),
             MarkerColor(LHCb::color(configtree.get(TString::Format("plots.%s.MColorData",nodename).Data(),"black"))),
             LineWidth  (static_cast<short>(configtree.get(TString::Format("plots.%s.LWidthData",nodename).Data(),1.f))),
             LineColor  (LHCb::color(configtree.get(TString::Format("plots.%s.LColorData",nodename).Data(),"black"))),
             DrawOption (static_cast<string>(configtree.get(TString::Format("plots.%s.OptionData",nodename).Data(),"e1p")).c_str())
             );

  //add optional legends
  //need to store them in a vector since the legends in the loop go out of scope and cause seg-fault when frame->Draw() is called
  vector< unique_ptr<TLegend> > legs;
  if(configtree.get_child_optional(TString::Format("plots.%s.legends",nodename).Data())){
    for(const auto& l : configtree.get_child(TString::Format("plots.%s.legends",nodename).Data())) {
      unique_ptr<TLegend> leg( new TLegend(l.second.get<double>("xlo"),
                                           1-gPad->GetTopMargin()-l.second.get<double>("dtop")-(l.second.get<double>("height")*textsize/(1-padsplit)),
                                           1-gPad->GetRightMargin()-l.second.get<double>("dright"),
                                           1-gPad->GetTopMargin()-l.second.get<double>("dtop"),
                                           static_cast<string>(l.second.get("header","")).c_str(),"brNDC"));
      leg->SetBorderSize(0);
      leg->SetFillColor(kWhite);
      leg->SetTextAlign(12);
      leg->SetFillStyle(0);
      leg->SetTextSize(l.second.get<double>("scale",1.f)*textsize/(1-padsplit));
      for(const auto& v : configtree.get_child( TString::Format("plots.%s.legends.%s",nodename,(l.first).data()).Data() ) ) {
        if( v.second.get_optional<string>("name") )//a bit dirty but it works
          leg->AddEntry(frame->findObject(v.second.get<string>("name").c_str()),
                        v.second.get<string>("label").c_str(),
                        v.second.get<string>("option","lpf").c_str()
                        );
      }
      legs.push_back(move(leg));
    }
  }
  for(const auto& leg : legs)
    frame->addObject(leg.get());

  //Hide potential exponent drawn at the right side of the axis
  TPaveText HideExpXP1(1.002-pad1.GetRightMargin(),pad1.GetBottomMargin(),1,pad1.GetBottomMargin()+0.05,"BRNDC");
  HideExpXP1.SetBorderSize(0);HideExpXP1.SetFillColor(kWhite);HideExpXP1.SetFillStyle(1001);HideExpXP1.SetTextColor(kWhite);
  HideExpXP1.AddText(" ");
  frame->addObject(&HideExpXP1);

  TString yaxistitle;
  const auto binsize = (configtree.get<double>(TString::Format("plots.%s.high",nodename).Data()) - configtree.get<double>(TString::Format("plots.%s.low",nodename).Data()))
                       /configtree.get<double>(TString::Format("plots.%s.bins",nodename).Data());
  if(boost::optional<string> ytitle = configtree.get_optional<string>(TString::Format("plots.%s.ytitle",nodename).Data()))yaxistitle = *ytitle;
  else if(frame->GetMaximum() > 1e+3 && frame->GetMaximum() < 1e+6)                  yaxistitle.Form("10^{3} Events/%.3g MeV",binsize);
  else if(frame->GetMaximum() > 1e+6)                                                yaxistitle.Form("10^{6} Events/%.3g MeV",binsize);
  else if(frame->GetMaximum() < 1e+3)                                                yaxistitle.Form("Events/%.3g MeV",binsize);
  else                                                                               yaxistitle = "";

  frame->GetYaxis()->SetTitle      (yaxistitle) ;
  frame->GetYaxis()->SetTitleSize  (textsize/(1-padsplit)) ;
  frame->GetYaxis()->SetLabelSize  (textsize/(1-padsplit)) ;
  frame->GetYaxis()->SetTitleOffset(static_cast<float>(configtree.get(TString::Format("plots.%s.yOffset",nodename).Data(),1.2))*(1-padsplit)) ;
  frame->GetYaxis()->SetRangeUser  (static_cast<float>(configtree.get(TString::Format("plots.%s.ymin",nodename).Data(),0.1)),
                                    static_cast<float>(configtree.get(TString::Format("plots.%s.ymax",nodename).Data(),frame->GetMaximum())));

  frame->SetName ("fit");
  frame->SetTitle("");
  frame->Draw    ();
  frame->Write   ();

  //go back to canvas and start using the second pad
  c1.cd();

  TPad pad2("pad2","pad2",padsplitmargin,padsplitmargin,1-padsplitmargin,padsplit);
  pad2.SetBorderSize  (0);
  pad2.SetLeftMargin  (Left_margin);
  pad2.SetRightMargin (Right_margin);
  pad2.SetTopMargin   (padsplitmargin);
  pad2.SetBottomMargin(Bigy_margin/padsplit);
  pad2.SetTickx();
  pad2.SetTicky();

  // Construct a histogram with the pulls of the data w.r.t the curve
  RooHist* hpull = frame->pullHist() ;
  hpull->SetMarkerSize (static_cast<Size_t>(configtree.get(TString::Format("plots.%s.MSizePull",nodename).Data(),1.f)));
  hpull->SetMarkerColor(LHCb::color(configtree.get(TString::Format("plots.%s.MColorPull",nodename).Data(),"azure+3")));
  hpull->SetLineWidth  (static_cast<Width_t>(configtree.get(TString::Format("plots.%s.LWidthPull",nodename).Data(),1.f)));
  hpull->SetLineColor  (LHCb::color(configtree.get(TString::Format("plots.%s.LColorPull",nodename).Data(),"azure+3")));
  hpull->SetDrawOption (static_cast<string>(configtree.get(TString::Format("plots.%s.OptionPull",nodename).Data(),"e1p")).c_str());

  // Create a new frame to draw the pull distribution and add the distribution to the frame
  RooPlot* frame2 = Observable->frame(configtree.get<double>(TString::Format("plots.%s.low",nodename).Data()),
                                 configtree.get<double>(TString::Format("plots.%s.high",nodename).Data()),
                                 configtree.get<int>   (TString::Format("plots.%s.bins",nodename).Data()));
  frame2->SetTitle(" ");
  frame2->GetXaxis()->SetTitleOffset(static_cast<float>(configtree.get(TString::Format("plots.%s.xOffset",nodename).Data(),1.05)));
  frame2->GetXaxis()->SetTitleSize  (textsize/padsplit);
  frame2->GetXaxis()->SetLabelSize  (textsize/padsplit);
  frame2->GetXaxis()->SetTitle      (static_cast<string>(configtree.get(TString::Format("plots.%s.xtitle",nodename).Data(),"M")).c_str());
  frame2->GetXaxis()->SetTickLength (frame->GetXaxis()->GetTickLength()/(padsplit/(1-padsplit)));
  frame2->GetYaxis()->SetLabelSize  (textsize/padsplit);
  frame2->GetYaxis()->SetTitleSize  (textsize/padsplit);
  frame2->GetYaxis()->SetNdivisions (static_cast<int>(configtree.get(TString::Format("plots.%s.xdiv",nodename).Data(),5)));
  frame2->GetYaxis()->SetTitleOffset(static_cast<float>(configtree.get(TString::Format("plots.%s.yOffset",nodename).Data(),1.2))*padsplit);
  frame2->GetYaxis()->SetTitle      (static_cast<string>(configtree.get(TString::Format("plots.%s.ytitlePull",nodename).Data(),"Pull ")).c_str());
  frame2->GetYaxis()->SetRangeUser  (-static_cast<float>(configtree.get(TString::Format("plots.%s.PullRange",nodename).Data(),4.8)),
                                     static_cast<float>(configtree.get(TString::Format("plots.%s.PullRange",nodename).Data(),4.8)));
  frame2->addPlotable(hpull,static_cast<string>(configtree.get(TString::Format("plots.%s.OptionPull",nodename).Data(),"e1p")).c_str());

  vector< unique_ptr<TLine> > plines;
  if(configtree.get_child_optional(TString::Format("plots.%s.pullLines",nodename).Data())){
    for(const auto& l : configtree.get_child(TString::Format("plots.%s.pullLines",nodename).Data())) {
      unique_ptr<TLine> pullline( new TLine(configtree.get<double>(TString::Format("plots.%s.low",nodename).Data()),l.second.get<double>("pull"),
                                            configtree.get<double>(TString::Format("plots.%s.high",nodename).Data()),l.second.get<double>("pull")));
      pullline->SetLineStyle(l.second.get("style",2));
      pullline->SetLineWidth(l.second.get("width",1));
      pullline->SetLineColor(LHCb::color(l.second.get("color","black")));
      pullline->SetDrawOption(static_cast<string>(l.second.get("option","l")).c_str());
      plines.push_back(move(pullline));
    }
  }
  for(const auto& line : plines){
    frame2->addObject(line.get());
  }

  TPaveText HideExpXP2(1.002-pad2.GetRightMargin(),pad2.GetBottomMargin(),1,pad2.GetBottomMargin()+0.05,"BRNDC");
  HideExpXP2.SetBorderSize(0);HideExpXP2.SetFillColor(kWhite);HideExpXP2.SetFillStyle(1001);HideExpXP2.SetTextColor(kWhite);
  HideExpXP2.AddText(" ");
  if(configtree.get_optional<bool>(TString::Format("plots.%s.HideExpX",nodename).Data()))
    frame2->addObject(&HideExpXP2);

  pad2.Draw();
  pad2.cd();
  frame2->Draw();
  frame2->SetName("pulls");
  c1.Update();
  frame2->Write();
  c1.Write();
  for(const auto& ex : configtree.get_child(TString::Format("plots.%s.outputformats",nodename).Data()))
    c1.SaveAs(TString::Format("%s/%s.%s",workdir.Data(),configtree.get<string>(TString::Format("plots.%s.plotname",nodename).Data()).data(),ex.first.data()).Data());

  //Small convergence check
  auto bins_gt_threshold = 0;
  //pulls should be within "pull_threshold" sigma
  auto pull_threshold = 4;
  //number of bins that are allowed to be bigger than pull_threshold
  auto n_bins_gt_pull_threshold = 2;
  const auto pulls = hpull->GetY();
  for(auto ij = 0; ij < configtree.get<double>(TString::Format("plots.%s.bins",nodename).Data()); ij++)
    if(fabs(pulls[ij]) > pull_threshold)bins_gt_threshold++;
  if(bins_gt_threshold >= n_bins_gt_pull_threshold)
    printf("\n\n\033[0;31mPulls too high! Please have a look\033[0m\n\n");

  return;
}
