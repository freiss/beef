//ROOT
#include "TROOT.h"
#include "TString.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TLeaf.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooKeysPdf.h"
#include "RooGaussian.h"
#include "RooFFTConvPdf.h"
//C++
#include <string>
#include <iostream>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace pt = boost::property_tree;
using namespace std ;
using namespace RooFit ;

void printhelp() 
{
  std::cout << "Usage: \n"
            << "createWorkspace -c <configfile> -i <inputfile> -t <treelocation> -o <outputfile> \n\n"
            << "Options: \n"
            << "-d <workdir (default=.)>\n"
            << "Output will be prepended with <workdir>\n" <<std::endl;
}



void print(const pt::ptree& configtree)
{
  pt::ptree::const_iterator end = configtree.end();
  for (pt::ptree::const_iterator it = configtree.begin(); it != end; ++it) {
    cout << it->first << ": " << it->second.get_value<string>() << endl;
    print(it->second);
  }
}


int main(int argc, char** argv){

  string config = "config/config.info";
  TString workdir = ".";
  TString infilename = "";
  TString outFileName = "";
  TString treeloc="";
  
  extern char* optarg;
  // extern int optind;
  // extern int optind;
  int ca;
  while ((ca = getopt(argc, argv, "c:d:i:o:t:h")) != -1)
    switch (ca) {
    case 'h':
      printhelp();
      return 0;
    case 'c':
      config = optarg;
      break;
    case 'd':
      workdir = optarg;
      break;
    case 'i':
      infilename = optarg;
      break;
    case 'o':
      outFileName = optarg;
      break;
    case 't':
      treeloc = optarg;
      break;
    }

  if(infilename=="" || outFileName=="" || config=="" || treeloc=="")
  {
    printhelp();
    return 1;
  }
  

  //check for file at different locations, i.e. the absolute path and a path with prepended workdir.
  //Prefer option with prepended workdir if both exist
  FileStat_t buffer;
  TString ifpn;
  //GetPathInfo is returned in the form of a FileStat_t structure (see TSystem.h). The function returns 0 in case of success and 1 if the file could not be stat'ed.
  if(!gSystem->GetPathInfo(infilename.Data(),buffer))ifpn = infilename;
  if(!gSystem->GetPathInfo(TString::Format("%s/%s",workdir.Data(),infilename.Data()).Data(),buffer))ifpn = TString::Format("%s/%s",workdir.Data(),infilename.Data());

  TFile* infile = TFile::Open( ifpn.Data() ,"READ");
  if(infile==nullptr)  {
    cerr << "inputfile ("<< infilename.Data() <<") not found" <<endl;
    return -1;
  }
  TTree* tree = (TTree*) infile->Get(treeloc);
  if(tree==nullptr){
    cerr<< "inputtree "<< treeloc <<" not found" << endl;
    return -1;
  }
  
  tree->SetBranchStatus("*",false);

  // Create empty property tree object
  pt::ptree configtree;
  // Parse the INFO into the property tree.
  pt::read_info(config, configtree);
  //print(configtree);
  
  vector<RooRealVar> variables;
  variables.reserve(configtree.get_child("variables").size());
  RooArgSet vars("variables");
  //RooCmdArg importCommands("MultiArg",0,0,0,0,0,0,0,0);
  //importCommands.setProcessRecArgs(true);

  //prepare observable
  auto obs_name = configtree.get<string>("observable.name");
  auto obs_low  = configtree.get<double>("observable.low");
  auto obs_high = configtree.get<double>("observable.high");
  RooRealVar Observable(obs_name.c_str(),obs_name.c_str(),obs_low,obs_high);
  //variables.push_back(Observable);
  //vars.add(variables.back());
  vars.add(Observable);
  boost::replace_all(obs_name, "[0]", "");
  tree->SetBranchStatus(obs_name.c_str(),true);

  //add "spectator" variables
  if(configtree.get_child_optional("variables")){
    for(const auto& v : configtree.get_child("variables")){
      // The data function is used to access the data stored in a node.
      string name=v.first;
      double low=v.second.get<double>("low");
      double high=v.second.get<double>("high");

      // check if there is an override for this variable
      string overloc="variables_override";
      if(configtree.find(overloc)!=configtree.not_found())
      {
        pt::ptree& overrides=configtree.get_child(overloc);
        if(overrides.find(name)!=overrides.not_found())
        {
          pt::ptree& override=overrides.get_child(name);
          std::cout << "overriding range for " << name << std::endl;
          low=override.get<double>("low");
          high=override.get<double>("high");
        }
      }

      RooRealVar myvar(name.c_str(),name.c_str(),low,high);
      variables.push_back(myvar);
      // here we refer to an entry in a vector
      // potentially dangerous if the vector is changed afterwards
      vars.add(variables.back());
      // for renaming the branches: DOES NOT WORK BECAUSE ROOSHIT
      //importCommands.addArg(RenameVariable(branch.c_str(),name.c_str()));

      // switch on the branches we need:
      boost::replace_all(name, "[0]", "");
      tree->SetBranchStatus(name.c_str(),true);
    }
  }

  cout << "Loading data ..." << endl;
  RooDataSet ds("ds","ds",vars,Import(*tree),Cut(configtree.get("basiccuts","").c_str())) ;

  if(ds.numEntries()==0) {
    cout << "0 events selected. Aborting." <<endl;
    return 1;
  }
  else cout << "Imported " << ds.numEntries() << " events." << endl;

  infile->Close();

  RooWorkspace w("w",true);
  w.import(ds); //,importCommands,RenameVariable("lab0_Cons_M[0]","mLb"));

  //prepare RooKeysPdfs (optional)
  if(configtree.get_child_optional("kdepdfs")){
    for(const auto& v : configtree.get_child("kdepdfs")){

      //get shapes from XFeed MC files - access the file and tree
      TString MCfilename;
      if(!gSystem->GetPathInfo(v.second.get<string>("filename").c_str(),buffer))MCfilename = v.second.get<string>("filename");
      if(!gSystem->GetPathInfo(TString::Format("%s/%s",workdir.Data(),v.second.get<string>("filename").c_str()).Data(),buffer))
        MCfilename = TString::Format("%s/%s",workdir.Data(),v.second.get<string>("filename").c_str());
      TFile* MCf = TFile::Open(MCfilename.Data());
      if(MCf==nullptr)  {
        cerr << "MC inputfile ("<< v.second.get<string>("filename").c_str() <<") not found" <<endl;
        return -1;
      }
      TTree* MCt = static_cast<TTree*> ( MCf->Get( v.second.get<string>("treepath").c_str() ) );
      if(tree==nullptr){
        cerr<< "MC inputtree "<< v.second.get<string>("treepath").c_str() <<" not found" << endl;
        return -1;
      }

      //load data from tree manually, since using import is in general not possible.
      //This is because the variable in MCtree should have the same name as the fit-observable
      TLeaf* MCl = MCt->GetLeaf( static_cast<string>(v.second.get("obsinfile",Observable.GetName())).c_str() );
      RooDataSet MCds("MCds","MCds",Observable);
      auto nEntriesMC = MCt->GetEntries();
      //for the resolution function's <Q> (see below)
      auto meanQ = 0.f; unsigned int acc = 0u;
      for (decltype(nEntriesMC) MCev = 0; MCev < nEntriesMC; MCev++){
        MCt->GetEntry(MCev);
        Observable.setVal(MCl->GetValue(0));
        if(obs_low < Observable.getVal() && Observable.getVal() < obs_high){
          MCds.add(Observable);
          meanQ += Observable.getVal();
          acc++;
        }
      }

      //make KDE-pdf from MC data
      RooKeysPdf kdepdf(v.first.c_str(),v.first.c_str(),Observable,MCds,
                        static_cast<RooKeysPdf::Mirror>(v.second.get("mirror",3)),
                        static_cast<double>(v.second.get("rho",1.f))
                        );

      //fold it with the detector resolution (optional)
      //it's not possible to directly fold a resolution function with mass dependent width https://root.cern.ch/phpBB3/viewtopic.php?t=14482
      //we keep the resolution fixed at sqrt(<Q>), where <Q> is the energy release of the sample mean of our keys pdf
      if (configtree.get_child_optional("resolution") && static_cast<bool>(v.second.get("fold",1))){
        meanQ /= static_cast<decltype(meanQ)>(acc);
        meanQ -= configtree.get("resolution.MassThreshold",0.f);
        auto resolution = sqrt(meanQ)*configtree.get("resolution.ResolutionScaling",1.f);
        printf("Folding %s with a %.2f MeV Gaussian resolution",kdepdf.GetName(),resolution);
        RooRealVar mg(TString::Format("%smg",kdepdf.GetName()).Data(),"mg",0);
        RooRealVar sg(TString::Format("%ssg",kdepdf.GetName()).Data(),"sg",resolution);
        RooGaussian gauss(TString::Format("%sres",kdepdf.GetName()).Data(),"Gaussian resolution",Observable,mg,sg);
        RooFFTConvPdf kxg(TString::Format("%sxg",kdepdf.GetName()).Data(),"kde (X) gauss",Observable,kdepdf,gauss);
        //w makes copy when importing. so we are safe when importing it in this scope and writing it later to file
        w.import(kxg);
      }
      else
        w.import(kdepdf);
      MCf->Close();
    }
  }

  w.Print();
  w.writeToFile(TString::Format("%s/%s",workdir.Data(),outFileName.Data()).Data());

  return 0;
}
