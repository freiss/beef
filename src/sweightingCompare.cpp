/*!                               
 *  @file      sweightingCompare.cpp
 *  @author    Florian Reiß
 *  @brief     Script to plot sweighted variables and MC, adapted from drawPlots.cpp
 *  @return    Returns the plots in the desired format.
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <unistd.h>  //to use getopt in the parser

//ROOT libraries
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TLegend.h>
#include <TMath.h>
#include "RooPlot.h"
#include "RooHist.h"
#include <TChain.h>
//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

using namespace std;
namespace pt = boost::property_tree;

//function to draw TH1D
void draw_TH1D(TFile *inFile_sw, TFile *inFile_mc, std::string outPath, const pt::ptree Canv, TString source){

  TCanvas *canv = new TCanvas("canv", "", Canv.get<double>("width"),Canv.get<double>("height"));
  TLegend *leg = new TLegend(0.65,0.73,0.86,0.87);

  string var_sw = Canv.get<string>("var_sw");
  string var_mc = Canv.get<string>("var_mc");
  TH1D *histo_mc;

  RooPlot *rooplot_sw = (RooPlot*) inFile_sw->Get(var_sw.data());

  RooHist * roohist = rooplot_sw->getHist(Canv.get<string>("h_sw").data());  

  int nrbins = roohist->GetN();
  double halfbinwidth = roohist->GetErrorX(0);
  double xlow = TMath::MinElement(nrbins,roohist->GetX()) -  halfbinwidth;
  double xupper = TMath::MaxElement(nrbins,roohist->GetX()) +  halfbinwidth;




    TTree *treeA = (TTree*)inFile_mc->Get(source);

    histo_mc  = new TH1D("histo_mc","", nrbins, xlow, xupper);
    treeA->Draw((Canv.get<string>("var_mc") + ">>histo_mc").c_str(),
              "",
              "");


  delete treeA;



  histo_mc->Scale(roohist->getFitRangeNEvt()/histo_mc->Integral());
  rooplot_sw->addTH1(histo_mc);


  if( boost::optional<string> leg_sw = Canv.get_optional<string>("leg_sw"))  leg->AddEntry(roohist, (*leg_sw).data(), "L");
  if( boost::optional<string> leg_mc = Canv.get_optional<string>("leg_mc"))  leg->AddEntry(histo_mc, (*leg_mc).data());

  if( boost::optional<string> title = Canv.get_optional<string>("title"))  rooplot_sw->SetTitle((*title).data());  
  if( boost::optional<string> xtitle = Canv.get_optional<string>("x_axisTitle"))  rooplot_sw->SetXTitle((*xtitle).data());
  if( boost::optional<string> ytitle = Canv.get_optional<string>("y_axisTitle"))  rooplot_sw->SetYTitle((*ytitle).data());

  rooplot_sw->Draw();
  if(Canv.get_optional<bool>("drawLegend")) leg->Draw();


  //---------------------//
  //  saving the canvas  //
  //---------------------//
  
  //I retrieve the list of formats to save
  std::string format_string = Canv.get<std::string>("formats");
  
  //now I loop over the output formats
  std::string format;
  std::string delimiter = ";";
  unsigned int pos = 0;
  
  //loop over formats
  while(((pos = format_string.find(delimiter)) != std::string::npos)
        && (format_string.size() != 0)){
    format = format_string.substr(0, pos);
    
    //gPad->Modified();

    gPad->Update();
    canv->SaveAs((outPath + Canv.get<std::string>("output_name") + "." + format).c_str());
    
    format_string.erase(0, pos + delimiter.length());
  }  //loop over formats 



  return; 
}




int main(int argc, char** argv){

	//gROOT->ProcessLine(".L lhcbstyle.C");
	//lhcbStyle();
	
  //-----------------------//
  //  parsing job options  //
  //-----------------------//
  
  std::string configFileName = "";
  std::string inPath = "";
  std::string outPath = "";
  
  extern char* optarg;
  
  int ca;
  
  //parsing the input options
  while ((ca = getopt(argc, argv, "c:i:o:h")) != -1){  
    switch (ca){
      
    case 'c':
      configFileName = optarg;
      break;
      
    case 'i':
      inPath = optarg;
      break;
      
    case 'o':
      outPath = optarg;
      break;

    case 'h':
      std::cout << "-- Help --" << std::endl;
      std::cout << "-c : configuration file name." << std::endl;
      std::cout << "-i : input path." << std::endl;
      std::cout << "-o : output path." << std::endl;
      std::cout << std::endl;
      exit(EXIT_FAILURE);
      break;
      
    default :
      std::cout << "Error: not recognized option. Type -h for the help." << std::endl;
      exit(EXIT_FAILURE);
      
    }  //switch (ca)
  }  //while ((ca = getopt(argc, argv, "i:o")) != -1)
  
  //printing of acquired options
  std::cout << "configFileName = " << configFileName << std::endl;
  std::cout << "inPath = " << inPath << std::endl;
  std::cout << "outPath = " << outPath << std::endl;
  
  //check of the acquired options
  if((inPath == "") || (outPath == "") || (configFileName == "")){
    std::cout << "Error: config file name or input/output paths not correctly set." << std::endl;
    exit(EXIT_FAILURE);
  }
  
  
  //----------------------------------//
  //  reading the configuration file  //
  //----------------------------------//
  
  //create empty property tree object
  pt::ptree configtree;
  
  //parse the INFO into the property tree.
  pt::read_info(configFileName, configtree);
	
  //--------------------------//
  //  loop over data to plot  //
  //--------------------------//
  
  //loop over input files
  for(pt::ptree::const_iterator File = configtree.get_child("Files").begin();
      File != configtree.get_child("Files").end(); ++File){
    
    //reading input file
    TFile* inFile_sw = TFile::Open((inPath + File->second.get<std::string>("file_name_sw")).c_str(),"READ");
    TFile* inFile_mc = TFile::Open(( File->second.get<std::string>("file_name_mc")).c_str(),"READ");    

    if(inFile_sw == nullptr || inFile_mc == nullptr){
      std::cout << "Error: input file does not exist." << std::endl;
      exit(EXIT_FAILURE); 
    }
    
    //loop over plots to draw
    for(pt::ptree::const_iterator Canv = File->second.get_child("Canvas").begin();
        Canv != File->second.get_child("Canvas").end(); ++Canv){
      

        draw_TH1D(inFile_sw,inFile_mc, outPath, Canv->second, File->second.get<std::string>("source"));
      

      
    } //loop over plots to draw
    
    //closing file
    inFile_sw->Close();
    inFile_mc->Close();
    
    //memory cleaning
    delete inFile_sw;
    delete inFile_mc;
  
  }  //loop over input files
  
  return 0;
  
}
