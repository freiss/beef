//ROOT
#include "TROOT.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TAxis.h"
#include "TH1.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"
#include "RooStats/SPlot.h"

//C++
#include <string>
#include <iostream>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>

namespace pt = boost::property_tree;
using namespace std ;
using namespace RooFit ;
using namespace RooStats ;


void printhelp() 
{
  std::cout << "Usage: \n" 
            << "weighted -c <configfile> -i <inputfile> -d <dataset> -o <outputfile> [-w <workspace> -s <splot>]\n\n" 
            << "default workspace name: w "<< std::endl;
}
void print(const pt::ptree& configtree)
{
  pt::ptree::const_iterator end = configtree.end();
  for (pt::ptree::const_iterator it = configtree.begin(); it != end; ++it) {
    cout << it->first << ": " << it->second.get_value<string>() << endl;
    print(it->second);
  }
}


int main(int argc, char** argv){

  string config = "";
  TString workdir = "";
  TString infilename = "";
  TString outFileName = "";
  TString dataset=""; 
  TString workspace="w";

    
  extern char* optarg;

  int ca;
  while ((ca = getopt(argc, argv, "c:d:i:o:t:h")) != -1)
    switch (ca) {
    case 'h':
      printhelp();
      return 0;
    case 'c':
      config = optarg;
      break;
    case 'i':
      infilename = optarg;
      break;
    case 'o':
      outFileName = optarg;
      break;    
    case 'd':
      dataset = optarg;
      break;
    case 'w':
      workspace = optarg;
      break;
    }
  

   if(infilename=="" || outFileName=="" || config=="" || dataset=="")
  {
    printhelp();
    return 1;
  }
  


  TFile* infile = TFile::Open( infilename.Data() ,"READ");
  if(infile==nullptr)  {
    cerr << "inputfile not found" <<endl;
    return -1;
  }
  RooWorkspace* ws = (RooWorkspace*) infile->Get(workspace);
  if(ws==nullptr){
    cerr<< "inputtree not found" << endl;
    return -2;
  }
  RooDataSet* ds = dynamic_cast<RooDataSet*>(ws->data(dataset));
  if(ds==nullptr){
    cerr<< "dataset not found" << endl;
    return -3;
  }
 
  // Create empty property tree object
  pt::ptree configtree;
  // Parse the INFO into the property tree.
  pt::read_info(config, configtree);
  // print(configtree);
  
 // get s-weighted data
  RooDataSet * dataw = new RooDataSet(ds->GetName(),ds->GetTitle(),ds,*ds->get(),0,configtree.get<string>("weights").c_str()) ; // need to configure this from property tree
 
  std::cout<<dataw->numEntries()<<std::endl;
  
 
   TFile* ofile = TFile::Open( outFileName ,"RECREATE");
  ofile->cd();

  // make the 1D plots
  BOOST_FOREACH(pt::ptree::value_type &v, configtree.get_child("plots")) {
    string variable=v.first.data();
    std::cout<<variable<<std::endl;
    RooRealVar* var = ws->var(variable.c_str());
    RooPlot* plot=var->frame();
    dataw->plotOn(plot,Binning(v.second.get<unsigned int>("bins")),DataError(RooAbsData::SumW2));
    plot->SetTitle(v.second.get<string>("title").c_str());
    plot->SetName(variable.c_str());
    plot->GetXaxis()->SetTitle(v.second.get<string>("xtitle").c_str());
    
    plot->Write();
  } //  end loop over plots
  
  // make 2D plots
   BOOST_FOREACH(pt::ptree::value_type &v, configtree.get_child("plots2D")) {
    string title=v.first.data();
    std::cout<<title<<std::endl;
    RooRealVar* varx=ws->var(v.second.get<string>("varx").c_str());
    RooRealVar* vary=ws->var(v.second.get<string>("vary").c_str());
    TH1* plot=dataw->createHistogram( title.c_str(),
                                       *varx,Binning(v.second.get<unsigned int>("binsx")),
                                       YVar(*vary,Binning(v.second.get<unsigned int>("binsy"))));

    
    //plot->GetXaxis()->SetTitle(v.second.get<string>("xtitle").c_str());
    
    plot->Write();
  } //  end loop over plots
  


  infile->Close();
  ofile->Close();
  
  return 0;
}
