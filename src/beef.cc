//from std
#include <string>
#include <iostream>
//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/replace.hpp>
//from ROOT
#include "TROOT.h"
#include "TStyle.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TSystem.h"
#include "TStopwatch.h"
//from ROOFIT
#include "RooWorkspace.h"
#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"
#include "RooDataHist.h"
//from ROOSTATS
#include "RooStats/SPlot.h"
//local
#include "../include/plotter.h"

namespace pt = boost::property_tree;
using namespace std ;
using namespace RooFit ;
using namespace RooStats ;

void printhelp() 
{
  std::cout << "Usage: \n"
            << "beef -c <configfile> -i <inputfile> -o <outputfile> \n\n"
            << "Options: \n"
            << "-d <workdir (default=.)>\n"
            << "-w <workspacename (default=w)> \n"
            << "Output will be prepended with <workdir>\n" <<std::endl;
}



void print(const pt::ptree& configtree)
{
  pt::ptree::const_iterator end = configtree.end();
  for (pt::ptree::const_iterator it = configtree.begin(); it != end; ++it) {
    cout << it->first << ": " << it->second.get_value<string>() << endl;
    print(it->second);
  }
}

int main(int argc, char** argv){

  string config = "";
  TString workdir = ".";
  TString infilename = "";
  TString outFileName = "";
  TString wspaceloc = "w";

  extern char* optarg;
  // extern int optind;
  // extern int optind;
  int ca;
  while ((ca = getopt(argc, argv, "c:d:i:o:w:h")) != -1)
    switch (ca) {
      case 'h':
        printhelp();
        return 0;
      case 'c':
        config = optarg;
        break;
      case 'd':
        workdir = optarg;
        break;
      case 'i':
        infilename = optarg;
        break;
      case 'o':
        outFileName = optarg;
        break;
      case 'w':
        wspaceloc = optarg;
        break;
    }

  if(infilename=="" || outFileName=="" || config=="")
  {
    printhelp();
    return 1;
  }
  
  TStopwatch clock;
  clock.Start();

  TFile* infile = TFile::Open( TString::Format("%s/%s",workdir.Data(),infilename.Data()).Data() ,"READ");
  RooWorkspace* w = (RooWorkspace*) infile->Get(wspaceloc) ;
  if(w==nullptr) {
    cout << "ERROR in retrieving workspace from root file" << endl;
    return 1;}

  // Get the model from the config file?
  pt::ptree configtree;
  // Parse the INFO into the property tree.
  pt::read_info(config, configtree);
  //print(configtree);

  // get dataset
  RooDataSet* dsFull = dynamic_cast<RooDataSet*>(w->data("ds")) ;
  if(dsFull==nullptr) {
    cout << "ERROR in reading dataset from saved workspace" << endl;
    return 2;}
  cout << dsFull->sumEntries() << " candidates in dataset" << endl;

  // apply cuts
  const string selection = static_cast<string>(configtree.get("selection",""));
  cout << "Selection: " << selection << endl;
  RooDataSet* ds = dynamic_cast<RooDataSet*>( dsFull->reduce(RooFit::Cut(selection.c_str()),
                                                             RooFit::Name(static_cast<string>(configtree.get("dsName","Dred")).c_str())
                                                             ) );
  auto nentries = ds->sumEntries();
  cout << nentries << " candidates selected. " << endl;

  // Build model from factory strings in fitconfig file
  cout  << "Model: "<< endl;
  BOOST_FOREACH(pt::ptree::value_type &v, configtree.get_child("model")) {
    string component=v.second.data();
    cout << component << endl;
    w->factory(component.c_str());
  }

  RooAbsPdf* modelPdf = w->pdf(configtree.get<string>("pdf").c_str());

  //get fitoptions
  RooLinkedList llist;
  vector<RooCmdArg*> args;
  args.push_back( new RooCmdArg( Save() ) );//we want to save the fitresult, so this is not optional
  if(configtree.get_optional<bool>("fitoptions.Extended"))        args.push_back( new RooCmdArg( Extended()) );
  if(configtree.get_optional<bool>("fitoptions.InitialHesse"))    args.push_back( new RooCmdArg( InitialHesse()) );
  if(configtree.get_optional<bool>("fitoptions.Minos"))           args.push_back( new RooCmdArg( Minos()) );
  if(configtree.get_optional<bool>("fitoptions.Verbose"))         args.push_back( new RooCmdArg( Verbose()) );
  if(configtree.get_optional<bool>("fitoptions.WarningsOff"))     args.push_back( new RooCmdArg( Warnings(false)) );
  if(configtree.get_optional<bool>("fitoptions.Timer"))           args.push_back( new RooCmdArg( Timer()) );
  if(configtree.get_optional<int> ("fitoptions.Strategy"))        args.push_back( new RooCmdArg( Strategy(configtree.get<int>("fitoptions.Strategy")) ) );
  if(configtree.get_optional<int> ("fitoptions.PrintLevel"))      args.push_back( new RooCmdArg( PrintLevel(configtree.get<int>("fitoptions.PrintLevel")) ) );
  if(configtree.get_optional<int> ("fitoptions.PrintEvalErrors")) args.push_back( new RooCmdArg( PrintEvalErrors(configtree.get<int>("fitoptions.PrintEvalErrors")) ) );
  if(configtree.get_optional<string>("fitoptions.MinimizerType"))
    args.push_back( new RooCmdArg( Minimizer(configtree.get<string>("fitoptions.MinimizerType").c_str(),configtree.get("fitoptions.MinimizerAlg","migrad").c_str()) ) );
  if(configtree.get_optional<int>("fitoptions.NumCPU"))
    args.push_back( new RooCmdArg( NumCPU(configtree.get<int>("fitoptions.NumCPU"),configtree.get("fitoptions.StratCPU",0)) ) );

  for (auto arg : args){
    //the next line forbids usage of RooCmdArgs as r-value,
    //also taking the adress of a reference and casting to TObject* can't be handled by RooFit, thus the pointers
    arg->setProcessRecArgs(true,false);
    llist.Add(arg);
  }

  RooFitResult* fitresult = nullptr;
  if(configtree.get_optional<bool>("fitoptions.FitAsHist")){
    //auto hist = ds->binnedClone(); <-- doesn't work...
    RooArgSet observables_for_hist;
    for(const auto& obs : configtree.get_child("observables")){
      RooRealVar* myobs = w->var(obs.first.c_str());
      myobs->setBins(configtree.get<int>(TString::Format("plots.%splot.bins",obs.first.c_str()).Data()));
      observables_for_hist.add(*myobs);
    }
    RooDataHist hist("hist","hist",observables_for_hist,*ds);
    fitresult = modelPdf->fitTo(hist,llist);
  }
  else
    fitresult = modelPdf->fitTo(*ds,llist);
  //delete pointers from heap
  for (auto arg : args)
    delete arg;
  args.clear();

  //Now we can get the sWeights. For this we need to set all shape parameters constant.
  //In the fitconfig, every extended parameter starts with N. This will be used below
  //yes this looks horrible, but didn't find elegant solution of how to set an arbitrary set of RooRealVars constant
  vector<TString> shape_varnames;
  vector<TString> extended_varnames;
  auto it_var = modelPdf->getVariables()->createIterator();
  auto var = it_var->Next();
  while (var){
    printf("%s ",var->GetName());
    bool obs_or_ext = false;
    for(const auto& obs : configtree.get_child("observables")){
      if (static_cast<TString>(var->GetName()) == obs.first.c_str()){
        printf("skipped (is observable)\n");
        obs_or_ext = true;
      }
    }
    for(const auto& extpar : configtree.get_child("extended")){
      if (static_cast<TString>(var->GetName()) == extpar.first.c_str()){
        extended_varnames.emplace_back(var->GetName());
        printf("used for sWeights\n");
        obs_or_ext = true;
      }
    }
    if(!obs_or_ext){
      shape_varnames.emplace_back( var->GetName() );
      printf("fixed\n");
    }
    var = it_var->Next();
  }

  for(auto varname : shape_varnames){
    auto shape_var = w->var(varname);
    shape_var->setConstant();
  }

  //put the yield-parameters into a RooArgSet which is input to the sPlot
  RooArgList extended_parameters;
  for(auto varname : extended_varnames)
    extended_parameters.add(*w->var(varname));

  SPlot("dummy","An SPlot",*ds, modelPdf, extended_parameters );

  infile->Close();
  TFile* outfile = TFile::Open( TString::Format("%s/%s",workdir.Data(),outFileName.Data()).Data() ,"RECREATE");
  outfile->cd();
  //create a new workspace to avoid seg-faults arising from cached pdfs like RooFFTConvPdf when trying to access the old workspace in the new file
  RooWorkspace ws("w",true);
  //sweights are saved by importing ds
  ws.import(*ds);
  ws.import(*fitresult);
  ws.import(*modelPdf);
  ws.Write();

  // create control plot
  if(configtree.get_child_optional("plots"))
    for(const auto& plot : configtree.get_child("plots"))
      plotter(ws,outfile,workdir,configtree,plot.first.c_str());

  outfile->Delete("*");
  outfile->Close("R");

  clock.Stop();
  clock.Print();

  return 0;
}
